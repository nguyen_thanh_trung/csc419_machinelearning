import numpy as np
import scipy.spatial.distance as ssd
from random import shuffle

def read_data(filename, isTraining):
   trainingSet = np.genfromtxt(filename, dtype=float, delimiter=',', skiprows=1)
   if not isTraining:
      shuffle(trainingSet)
      
   trainingData = []
   trainingClass = []

   for data in trainingSet:
       trainingData.append(data[:-1])
       trainingClass.append(data[-1])
   return np.array(trainingData), np.array(trainingClass)

def KNN(k, trainingData, testingData, trainingClass, calDistMethod, classifyMethod, weight):
    predictedClass = []
    
    for testingIndex,testingValue in enumerate(testingData):
        distances = []

        for trainingIndex, trainingValue in enumerate(trainingData):
            distances.append((computeDistance(testingValue, trainingValue, calDistMethod), trainingIndex))

        k_nn = sorted(distances)[:k]
        predictedClass.append(classify(k_nn, trainingClass, classifyMethod,weight))

    return predictedClass

def computeDistance(testingData, trainingData, calDistMethod):
   if calDistMethod == 0:
      return ssd.euclidean(testingData, trainingData)
   elif calDistMethod == 1:
      return ssd.cityblock(testingData, trainingData)
   else:
      return 0

def classify(k_nn, trainingClass, classifyMethod, weight):
   if classifyMethod == 1:
      dist,index = k_nn[0]
      return trainingClass[index]

   elif classifyMethod == 0:
      label = dict()
      
      for dist, index in k_nn:
         vote = 0
         if weight == "distance":
            vote = 1/dist if (dist != 0) else 0
         elif weight == "uniform":
            vote = 1
               
         if trainingClass[index] in label:
            label[trainingClass[index]] += vote
         else:
            label[trainingClass[index]] = vote

      selectedIndex = -1
      maxValue = -1
      for index, value in label.iteritems():
         if (value > maxValue):
            maxValue = value
            selectedIndex = index

      return selectedIndex
         

def main(option, kneighbour, calDistMethod, classifyMethod, weight):
   filename = ""
   if option == 0:
      filename = "face"
   elif option == 1:
      filename = "iris"
   elif option == 2:
      filename = "newiris"
   else:
      filename = "hhh"

   trainingData, trainingClass = read_data(filename + "-train.csv", True)
   testingData, testingClass = read_data(filename + "-test.csv", False)
   predictedClass = KNN(kneighbour, trainingData, testingData, trainingClass, calDistMethod, classifyMethod, weight)

   correct = 0;
   for index in range(len(predictedClass)):
      if (predictedClass[index] == testingClass[index]):
         correct = correct + 1

   percentCorrect = float(correct) / len(predictedClass)
   print "Correctly Classified Instances = " + str(percentCorrect)
   return trainingData, trainingClass, testingData, testingClass, predictedClass


def add(a , b):
    return (a+b)
   
