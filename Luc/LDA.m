%one instance per row
function w = LDA(data, class)    
    
    %init
    numClass = size(unique(class),1);
    dim = size(data,2);  
    sw = zeros(dim,dim);
    sb = zeros(dim,dim);
    
    %mean of all class
    m = mean(data,1);
    
    %compute sw sb
    for c = unique(class')
        dataInClass = data(class == c, :);
        sw = sw + cov(dataInClass);        
        mc = mean(dataInClass,1);%mean of class c
        sb = sb + size(dataInClass,1)*((mc-m)'*(mc-m));
    end    
    
    SWinv = inv(sw);
    SWinvSB = SWinv*sb;        
    [V, D] = eig(SWinvSB);    
    [~, order] = sort(diag(D),'descend');  %sort eigenvalues in descending order
    w = V(:,order);
    w = w(:,1:(numClass-1));
end