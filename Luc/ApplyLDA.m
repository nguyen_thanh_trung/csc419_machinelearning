data = csvread('InitialData/data.csv');
class = data(:,size(data,2));
data = data(:,1:size(data,2)-1);
w = LDA(data, class);    
newData = data*w;
className = cell(size(class,1),1);
file = fopen('LDAdata.csv','w');
for i=1:size(newData,1)
    for j=1:size(newData,2)
        fprintf(file,'%f,',newData(i,j));
    end
    fprintf(file,'class%d\n',class(i));
end
fclose(file);