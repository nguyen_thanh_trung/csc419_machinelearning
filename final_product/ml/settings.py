import os

settings = {}

settings['PROJECT_PATH'] = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
settings['DATA_PATH'] = os.path.join(settings['PROJECT_PATH'], 'data')
settings['MATLAB_PATH'] = os.path.join(settings['PROJECT_PATH'], 'matlab')
settings['PYTHON_PATH'] = os.path.join(settings['PROJECT_PATH'], 'ml')
settings['TRAINING_SIZE'] = 4

settings['ALGORITHM_ID'] = {
    'PREPROCESS': {
        'RAW': 0,
        'PCA': 1,
        'LDA': 2,
        'FFT': 3,
        'OUTLINE': 4,
    },
    'CLASSIFICATION': {
        'KNN': 1,
        'NEURAL': 2,
    }
}

print('Loaded settings: ', settings)
