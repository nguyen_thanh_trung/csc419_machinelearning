import os
from ml.classification.knn.knn import knn
from ml.misc.matlab import run_matlab
from ml.settings import settings
import numpy as np

def find(test_file_path, preprocess, algorithm):
    training_set = None
    test_data = None
    if preprocess == settings['ALGORITHM_ID']['PREPROCESS']['FFT']:
        training_set = np.genfromtxt(
            os.path.join(settings['DATA_PATH'], 'FFT', 'train.csv'),
            delimiter=',',
            skiprows=1
        )
        tmp_path = os.path.join(settings['DATA_PATH'], 'tmp', 'data.csv')
        run_matlab('convert_fft', "'" + test_file_path + "', '"
                                  + tmp_path + "'")
        test_data = np.genfromtxt(tmp_path, delimiter=',', skiprows=0)
    elif preprocess == settings['ALGORITHM_ID']['PREPROCESS']['PCA']:
        training_set = np.genfromtxt(
            os.path.join(settings['DATA_PATH'], 'PCA', 'train.csv'),
            delimiter=',',
            skiprows=1
        )
        tmp_path = os.path.join(settings['DATA_PATH'], 'tmp', 'data.csv')
        vlead_path = os.path.join(settings['DATA_PATH'], 'PCA', 'V_lead.csv')
        mean_path = os.path.join(settings['DATA_PATH'], 'PCA', 'mean.csv')
        run_matlab('convert_pca', "'" + test_file_path + "', '"
                                  + tmp_path + "', '"
                                  + vlead_path + "', '"
                                  + mean_path + "'")
        test_data = np.genfromtxt(tmp_path, delimiter=',', skiprows=0)
    else:
        print('Algorithm not yet implemented', algorithm)

    if algorithm == settings['ALGORITHM_ID']['CLASSIFICATION']['KNN']:
        return knn(training_set, test_data)
    else:
        print('Algorithm not yet implemented ', algorithm)
