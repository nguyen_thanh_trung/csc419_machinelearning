import os
from ml.main.find import find
from ml.settings import settings

correct = 0
total = 0

for i in range(1,41):
    path = os.path.join(settings['DATA_PATH'], 'TestingData', 's' + str(i))
    files = os.listdir(path)
    for file in files:
        res = find(
            os.path.join(path, file),
            settings['ALGORITHM_ID']['PREPROCESS']['PCA'],
            settings['ALGORITHM_ID']['CLASSIFICATION']['KNN'])
        total += 1
        if res == i:
            correct += 1
            print 'Correct: ', correct, '/', total

print 'Correct: ', correct, '/', total
