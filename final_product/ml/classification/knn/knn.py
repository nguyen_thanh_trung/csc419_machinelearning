import numpy as np
import scipy.spatial.distance as ssd


def knn(trainingSet, testingValue, k=1, distMethod=0, classifyMethod=1, weight="uniform"):
    trainingData = []
    trainingClass = []
    for data in trainingSet:
        trainingData.append(data[:-1])
        trainingClass.append(data[-1])

    trainingData = np.array(trainingData)
    trainingClass = np.array(trainingClass)

    distances = []
    for trainingIndex, trainingValue in enumerate(trainingData):
        distances.append(
            (computeDistance(testingValue, trainingValue, distMethod), trainingIndex)
        )

    k_nn = sorted(distances)[:k]
    return classify(k_nn, trainingClass, classifyMethod, weight)


def computeDistance(testingData, trainingData, calDistMethod):
    if calDistMethod == 0:
        return ssd.euclidean(testingData, trainingData)
    elif calDistMethod == 1:
        return ssd.cityblock(testingData, trainingData)
    else:
        return 0


def classify(k_nn, trainingClass, classifyMethod, weight):
    if classifyMethod == 1:
        dist,index = k_nn[0]
        return trainingClass[index]
    elif classifyMethod == 0:
        label = dict()
        for dist, index in k_nn:
            vote = 0
            if weight == "distance":
                vote = 1/dist if (dist != 0) else 0
            elif weight == "uniform":
                vote = 1

            if trainingClass[index] in label:
                label[trainingClass[index]] += vote
            else:
                label[trainingClass[index]] = vote

        selectedIndex = -1
        maxValue = -1
        for index, value in label.iteritems():
            if (value > maxValue):
                maxValue = value
                selectedIndex = index

        return selectedIndex
