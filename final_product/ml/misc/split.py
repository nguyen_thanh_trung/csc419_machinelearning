from os import listdir
from os.path import isfile, join
from random import shuffle
from subprocess import call
from ml.settings import settings

directories = [dir
               for dir in listdir(join(settings['DATA_PATH'], 'InitialData'))
               if not isfile(join(settings['PROJECT_PATH'], dir))]

call(['rm', '-rf', join(settings['DATA_PATH'], 'TestingData')])
call(['rm', '-rf', join(settings['DATA_PATH'], 'TrainingData')])

call(['mkdir', join(settings['DATA_PATH'], 'TrainingData')])
call(['mkdir', join(settings['DATA_PATH'], 'TestingData')])

for directory in directories:
    file_id = [i for i in range(1,11)]
    shuffle(file_id)

    call(['mkdir', join(settings['DATA_PATH'], 'TrainingData', directory)])
    call(['mkdir', join(settings['DATA_PATH'], 'TestingData', directory)])

    for idx, i in enumerate(file_id):
        src = join(settings['DATA_PATH'], 'InitialData', directory, str(i) + '.pgm')
        if idx < settings['TRAINING_SIZE']:
            dst = join(settings['DATA_PATH'], 'TrainingData', directory, str(i) + '.pgm')
        else:
            dst = join(settings['DATA_PATH'], 'TestingData', directory, str(i) + '.pgm')
        call(['cp', src, dst])
