from subprocess import call, CalledProcessError
from ml.settings import settings


def run_matlab(script_path, params):
    command = '/home/rr/Tools/MATLAB/R2012a/bin/matlab -nodisplay -nosplash -nodesktop -nojvm -r "addpath(\'' \
            + settings['MATLAB_PATH'] + '\');' \
            + script_path + '(' + params + ')' \
            + '; exit"'
    print('Execute matlab using command: ', command)
    call(command, shell=True)
