function convert(sourceDir, targetFile)
    images = [];
    for i = 1:40
        for j = 1:10
            srcFile = sprintf('%s/s%d/%d.pgm', sourceDir, i, j);
            if exist(srcFile, 'file') == 2
                img = imread(srcFile);
                img_size = size(img, 1) * size(img, 2);
                img = reshape(img, 1, img_size);
                images = [images; img];
            end
        end
    end
    csvwrite(targetFile, images);
end
