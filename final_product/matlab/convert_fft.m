function convert_fft(sourceFile, targetFile)
    img = imread(sourceFile);
    img_fft = fftshift(log(fft2(img)));
    img_fft_cut = img_fft(53:58, 44:48);
    total_size = size(img_fft_cut, 1) * size(img_fft_cut, 2);
    img_fft_cut = reshape(img_fft_cut, 1, total_size);

    csvwrite(targetFile, real(img_fft_cut));
end
