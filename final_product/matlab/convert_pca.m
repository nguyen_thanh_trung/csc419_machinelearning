function convert_pca(sourceFile, targetFile, vleadFile, meanFile)
    vlead = csvread(vleadFile);
    m = csvread(meanFile);

    img = imread(sourceFile);
    img_size = size(img, 1) * size(img, 2);
    img = reshape(img, 1, img_size);

    test_images = double(img');

    shifted_test_images = test_images - repmat(m, 1, 1);
    project_test_images = vlead * shifted_test_images;

    csvwrite(targetFile, real(project_test_images'));
end
