clear all; close all; clc;

training_size = 4;
dimensions = 50;

train_images = [];
for i = 1:40
    for j = 1:10
        srcFile = sprintf('data/TrainingData/s%d/%d.pgm', i, j);
        if exist(srcFile, 'file') == 2
            img = imread(srcFile);
            img_size = size(img, 1) * size(img, 2);
            train_images = [train_images; reshape(img, 1, img_size)];
        end
    end
end
train_images = double(train_images');

m = mean(train_images, 2); %compute mean vector
shifted_images = train_images - repmat(m, 1, 40*training_size); %compute shifted images

c = 1/(40*training_size)*shifted_images'*shifted_images; %covariance matrix
[V,D] = eig(c);
V1 = shifted_images*V;
V_lead = V1(:,1:dimensions); %leading eigenvectors 

project_images = V_lead' * shifted_images; %project the images


test_images = [];
for i = 1:40
    for j = 1:10
        srcFile = sprintf('data/TestingData/s%d/%d.pgm', i, j);
        if exist(srcFile, 'file') == 2
            img = imread(srcFile);
            img_size = size(img, 1) * size(img, 2);
            test_images = [test_images; reshape(img, 1, img_size)];
        end
    end
end
test_images = double(test_images');
shifted_test_images = test_images - repmat(m, 1, 40*(10-training_size)); %compute shifted test images
project_test_images = V_lead' * shifted_test_images; %project the test images


csvwrite('data/PCA/train_unlabeled.csv', project_images');
csvwrite('data/PCA/test_unlabeled.csv', project_test_images');
csvwrite('data/PCA/V_lead.csv', V_lead');
csvwrite('data/PCA/mean.csv', m);
