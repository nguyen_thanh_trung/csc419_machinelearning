function extract_fft(sourceDir, targetFile)
    images = [];
    for i = 1:40
        for j = 1:10
            srcFile = sprintf('%s/s%d/%d.pgm', sourceDir, i, j);
            if exist(srcFile, 'file') == 2
                img = imread(srcFile);
                img_fft = fftshift(log(fft2(img)));
                img_fft_cut = img_fft(53:58, 44:48);
                total_size = size(img_fft_cut, 1) * size(img_fft_cut, 2);
                img_fft_cut = reshape(img_fft_cut, 1, total_size);
                images = [images; real(img_fft_cut)];
            end
        end
    end
    csvwrite(targetFile, images);
end
