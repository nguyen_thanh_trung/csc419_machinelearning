#include <fstream>
#include <iostream>
using namespace std;

const int NROW = 4;

int main() {
    fstream orig, out;
    orig.open("train_unlabeled.csv", fstream :: in);
    out.open("train.csv", fstream :: out);

    string s; orig >> s;
    int cnt = 0;
    for(int i = 0; i < s.length(); ++i)
        if (s[i] == ',')
            out << 'x' << (cnt++) << ',';
    out << 'x' << (cnt++) << ",class\n";

    int low, high;
    for(int person = 0; person < 40 * NROW; ++person) {
        out << s << ",c" << person/NROW + 1 << endl;
        orig >> s;
    }
    out.close(); orig.close();
}
