function [training_set, test_set]=db_load(training_size);
% We load the database the first time we run the program.

persistent loaded;
persistent v;
persistent u;
if(isempty(loaded))
    v=zeros(10304, 40*training_size);
    u=zeros(10304, 40*(10-training_size));
    for i=1:40
        cd(strcat('s',num2str(i)));
        %read the training images
        for j=1:training_size
            a=imread(strcat(num2str(j),'.pgm'));
            v(:,(i-1)*training_size+j)=reshape(a,size(a,1)*size(a,2),1);
        end
        
        %read the test images
        for j=training_size+1:10
            a=imread(strcat(num2str(j),'.pgm'));
            u(:,(i-1)*(10-training_size)+j-training_size)=reshape(a,size(a,1)*size(a,2),1);
        end
        
        cd ..
    end
end
loaded=1;  % Set 'loaded' to aviod loading the database again. 
training_set=v;
test_set= u;