function accuracy=knn(dimensions, k);
training_size = 4;
[training_images, test_images] = db_load(training_size);

%compute pca
m = mean(training_images, 2); %compute mean vector
shifted_images = training_images - repmat(m, 1, 40*training_size); %compute shifted images

[evectors, score, evalues] = princomp(training_images'); %compute the eigenvectors and eigenvalues
lead_evectors = evectors(:, 1:dimensions); %get the leading eigenvectors
project_images = lead_evectors' * shifted_images; %project the images

%compute knn
shifted_test_images = test_images - repmat(m, 1, 40*(10-training_size)); %compute shifted test images
project_test_images = lead_evectors' * shifted_test_images; %project the test images

true_detection = 0;

for i=1:40*(10-training_size)
    similarity_score(i,:) = arrayfun(@(n) norm(project_images(:,n) - project_test_images(:,i)), 1:40*training_size); %compute the similarity score
    [sorted_score, sorted_ix]= sort(similarity_score(i,:),'ascend'); %sort the similarity score 
    classes=zeros(1, 40);
    
    %find the k images with highest similarity
    for j=1:k
        index = ceil(sorted_ix(j)/ training_size);
        classes(index)=classes(index)+1; 
    end
    
   [C, nearest_class(:,i)] = max(classes); %find the dominant class among k images
   
   %validate the detection
   if nearest_class(:,i) == ceil(i/(10-training_size))
       true_detection = true_detection + 1; 
   end;
   
end

%compute the accuracy
accuracy = true_detection/(40*(10-training_size));
