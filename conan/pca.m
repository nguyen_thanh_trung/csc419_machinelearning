clear;
k = 3;

dimensions = 300;
training_size = 4;
[training_images, test_images] = db_load(training_size);

%compute pca
m = mean(training_images, 2); %compute mean vector
shifted_images = training_images - repmat(m, 1, 40*training_size); %compute shifted images

for j=1:40*training_size
    x(:,j) = training_images(:,j) - m;
end

%compute pca manually
%{
%c=cov(shifted_images);
c = 1/(40*training_size)*shifted_images'*shifted_images; %covariance matrix
[V,D] = eig(c);
V1 = shifted_images*V;
V_lead = V1(:,end:-1:end-(dimensions-1)); %leading eigenvectors 
%}

[evectors, score, evalues] = princomp(training_images'); %compute the eigenvectors and eigenvalues
lead_evectors = evectors(:, 1:dimensions); %get the leading eigenvectors
project_images = lead_evectors' * shifted_images; %project the images


%compute knn
shifted_test_images = test_images - repmat(m, 1, 40*(10-training_size)); %compute shifted test images
project_test_images = lead_evectors' * shifted_test_images; %project the test images

classes=zeros(40*(10-training_size), 40);
for i=1:40*(10-training_size)
    similarity_score(i,:) = arrayfun(@(n) norm(project_images(:,n) - project_test_images(:,i)), 1:40*training_size); %compute the similarity score
    [sorted_score(i,:), sorted_ix(i,:)]= sort(similarity_score(i,:),'ascend'); %sort the similarity score 
    
    %find the k images with highest similarity
    for j=1:k
        index = ceil(sorted_ix(i,j)/ training_size);
        classes(i,index)=classes(i, index)+1; 
    end
    
   [C, nearest_class(:,i)] = max(classes(i,:)); %find the dominant class among k images
end

similarity_score = similarity_score';

%{   
% display the result
figure, imshow([input_image reshape(images(:,match_ix), image_dims)]);
title(sprintf('matches %s, score %f', filenames(match_ix).name, match_score));
%}
