[U, S, V] = svd(all_data, 0);
people = S * V';

feature = 40;
idx = 0;
train_images = [];
test_images = [];
for person = 1:40
    for id = 1:10
        idx = idx + 1;
        total_size = feature * size(people(:,idx), 2);
        cur_person = reshape(people(1:feature,idx), 1, total_size);
        if id <= 4
            train_images = [train_images; real(cur_person)];
        else
            test_images = [test_images; real(cur_person)];
        end
    end
end
csvwrite('fft_data/edge_train.csv', train_images);
csvwrite('fft_data/edge_test.csv', test_images);
