function [cA,cH,cV,cD]=haar2D(xin)

%{
2-D Haar transform: 1-level only

input:
    xin: [M x N], M,N even

output:
    cA: average image, [M/2 x N/2]
    cH: horizontal edges, [M/2 x N/2]
    cV: vertical edges, [M/2 x N/2]
    cD: diagonal edges, [M/2 x N/2]
%}

    n=size(xin);
    vr=2*ones(floor(n(1))/2,1);
    vc=2*ones(floor(n(2))/2,1);
    x=mat2cell(double(xin(1:2*numel(vr),1:2*numel(vc))),vr,vc);
    cA=cell2mat(cellfun(@(x)(sum(x(:))/2),x,'uniformoutput',false));
    cH=cell2mat(cellfun(@(x)(sum(x(1,:)-x(2,:))/2),x,'uniformoutput',false));
    cV=cell2mat(cellfun(@(x)(sum(x(:,1)-x(:,2))/2),x,'uniformoutput',false));
    cD=cell2mat(cellfun(@(x)(sum(diag(x)-diag(fliplr(x)))/2),x,'uniformoutput',false));

end
