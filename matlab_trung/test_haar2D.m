close all; clear all; clc;

idx = 0;
for person = 1:2
    for id = 1:4
        idx = idx + 1
        filename = sprintf('InitialData/s%d/%d.pgm', person, id);
        face = imread(filename);
        [cA, cH, cV, cD] = haar2D(face);

        nbcol = size(colormap(gray),1);
        cod_cH1 = wcodemat(cH,nbcol);
        cod_cV1 = wcodemat(cV,nbcol);
        cod_edge=cod_cH1+cod_cV1;
        
        figure(idx)
        subplot(2,2,1), imshow(uint8(face));
        subplot(2,2,2), imshow(uint8(cod_cH1));
        subplot(2,2,3), imshow(uint8(cod_cV1));
        subplot(2,2,4), imshow(uint8(cod_edge));
        
        total_size = size(cod_edge, 1) * size(cod_edge, 2);

        all_data(:,idx) = reshape(cod_edge, 1, total_size);
    end
end
