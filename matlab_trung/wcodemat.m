function result = wcodemat(X, scale)
  
Xmin = min(min(X));
  Xmax = max(max(X));
  Xrange = Xmax - Xmin;
  

  result = round(Xrange.\(X-Xmin)*(scale-1)+1);

end
