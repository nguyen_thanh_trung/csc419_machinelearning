clear all; close all; clc;

img = imread('InitialData/s4/1.pgm');
size(img)
img_fft = fft2(img);
img = uint8(real(ifft2(img_fft)));
size(img)

figure(1),
subplot(1,2,1), imshow(img), colormap(gray)
subplot(1,2,2), pcolor(log(abs(fftshift(img_fft)))), shading interp, colormap(hot);

[m,n] = size(img);
img_fft = fftshift(img_fft);

for i = 1:m
    for j = 1:n
        if 47 <= i && i <= 63 && 38 <= j && j <= 54
            img_fft_filtered(i,j) = img_fft(i,j);
        else
            img_fft_filtered(i,j) = 0;
        end
    end
end
img_fft_filtered = fftshift(img_fft_filtered);

figure(2),
subplot(1,2,1), pcolor(log(abs(fftshift(img_fft_filtered)))), shading interp, colormap(hot);
subplot(1,2,2), imshow(uint8(real(ifft2(img_fft_filtered)))), colormap(gray);
