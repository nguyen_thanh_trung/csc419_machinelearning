train_images = [];
test_images = [];
for person = 1:40
    for img_id = 1:10
        file_path = sprintf('InitialData/s%d/%d.pgm', person, img_id);
        img = imread(file_path);
        img_fft = fftshift(log(fft2(img)));
        img_fft_cut = img_fft(53:58, 44:48);
        total_size = size(img_fft_cut, 1) * size(img_fft_cut, 2);
        img_fft_cut = reshape(img_fft_cut, 1, total_size);
        if img_id <= 4
            train_images = [train_images; real(img_fft_cut)];
        else
            test_images = [test_images; real(img_fft_cut)];
        end
    end
end
csvwrite('fft_data/data_train.csv', train_images);
csvwrite('fft_data/data_test.csv', test_images);


% (52:58, 43:49) --> 80%
% (53:58, 44:49) --> 82%
% (52:57, 43:48) --> 78.75%
% (53:57, 44:48) --> 80.8%
% (53:58, 44:48) --> 82%
