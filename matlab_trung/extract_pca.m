clear all; close all; clc;
train_images = [];
test_images = [];

for person = 1:40
    for img_id = 1:10
        file_path = sprintf('InitialData/s%d/%d.pgm', person, img_id);
        img = imread(file_path);
        img_size = size(img, 1) * size(img, 2);
        if img_id <= 4
            train_images = [train_images; reshape(img, 1, img_size)];
        else
            test_images = [test_images; reshape(img, 1, img_size)];
        end
    end
end

n = size(train_images, 1);
X = double(train_images');
X = X - repmat(mean(X, 2), 1, n);
C = (1 / (n-1)) * X' * X;

[V, D] = eig(C);
Y = V' * X';

csvwrite('fft_data/data_train_pca.csv', train_images);
csvwrite('fft_data/data_test_pca.csv', test_images);
