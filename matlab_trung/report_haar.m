clear all; close all; clc;

img = imread('InitialData/s4/3.pgm');

[cA, cH, cV, cD] = haar2D(img);

nbcol = size(colormap(gray),1);
cod_cH1 = wcodemat(cH,nbcol);
cod_cV1 = wcodemat(cV,nbcol);
cod_edge=cod_cH1+cod_cV1;
        
subplot(1,2,1), imshow(uint8(img));
subplot(1,2,2), imshow(uint8(cod_edge));
