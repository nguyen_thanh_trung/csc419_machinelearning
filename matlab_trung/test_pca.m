clear all;
close all;
clc;

X = rand(100);
n = 100;
X = X - repmat(mean(X, 2), 1, n);
C1 = 1/n * X * X';
C2 = 1/n * X' * X;

[V1, D1] = eig(C1);
[V2, D2] = eig(C2);

max(max(V1 - X*V2'))
max(max(V1' - X*V2))

max(max(D1.^2 - D2))
