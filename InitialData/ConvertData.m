data = zeros(400,953);
temp = zeros(1,953);

for label=1:40
    for i=1:10
        fname = strcat('s',num2str(label),'/',num2str(i),'.pgm');
        img = imread(fname);
        img = imresize(img,0.3);
        img = histeq(img);
        temp(1,1:953-1) = img(:)';
        temp(1,953) = label;
        data((label-1)*10 + i,:) = temp;
    end
end  
    
csvwrite('data.csv',data);