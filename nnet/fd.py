import h5py
import numpy as np
from sklearn import svm, ensemble
from sklearn import cross_validation as cv
from sklearn.decomposition import PCA
from nnet import NeuralNet
from sklearn import preprocessing

with h5py.File('train.p', 'r') as f:
	data_train = f['data'].value[:, :].astype(np.int)

with h5py.File('test.p', 'r') as f:
	data_test = f['data'].value[:, :].astype(np.int)

########## SPLIT TRAIN AND VALIDATION ##########
x = data_train[:, :-1].astype('float')
y = data_train[:, -1]
x_test = data_test[:, :-1].astype('float')
y_test = data_test[:, -1]
y = y - y.min()
y_test = y_test - y_test.min()

######### PCA #########
# pca = PCA(400)
# pca.fit(x)
# x = pca.transform(x)
# x_test = pca.transform(x_test)

######### STANDARDIZATION #########
scaler = preprocessing.StandardScaler().fit(np.vstack((x, x_test)))
x = preprocessing.scale(x)
x_test = preprocessing.scale(x_test)

######### FIT MODEL #########
n_features = x.shape[1]
n_hiddens = 100
n_labels = 40
lmbda = 0.01 #best 0.01
maxiter = 100

nn = NeuralNet(n_features, n_hiddens, n_labels, lmbda, maxiter)
nn.fit(x, y)

######### PREDICT ##########
p = nn.predict(x_test)
acc =  np.mean(p == y_test)* 1.0
print 'Accuracy %.2f' % acc
