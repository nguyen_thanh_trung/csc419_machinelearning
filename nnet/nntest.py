import numpy as np
from nnet import NeuralNet
from scipy.io import loadmat

# Load data
data = loadmat('v1/ex4data1.mat')
X = data['X']			# X: training data, dimension (n_instances x n_features)
y = data['y'].flatten() # y: classes, 		dimension (n_instances x 1)
y = y - y.min() 		# convert classes to 0-index

# Neural Network Configuration
n_features = 400
n_hiddens = 25
n_labels = 10
lmbda = 1.0
maxiter = 50

# Create a neural network
nn = NeuralNet(n_features, n_hiddens, n_labels, lmbda, maxiter)

# Print information about the config of the network
print(nn)

# Fit the model to the training data
nn.fit(X, y)
# Use the trained model to predict
pred = nn.predict(X)
# Prediction accuracy
acc = np.mean(pred == y) * 100
print("Accuracy %.2f%%" % acc)