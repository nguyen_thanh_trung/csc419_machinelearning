from nnCostFunction import *
from randInitWeights import *
from nn_helpers import *
from predict import *
import numpy as np
from scipy.io import loadmat
from scipy.optimize import fmin_cg

def init_weights(input_size, hidden_size, n_labels):
    initial_W1 = randInitWeights(input_size, hidden_size)
    initial_W2 = randInitWeights(hidden_size, n_labels)
    initial_nn_params = np.hstack((initial_W1.T.ravel(), initial_W2.T.ravel()))
    return initial_nn_params

def test_nn():
    input_size = 400
    hidden_size = 25
    n_labels = 10
    lmbda = 0

    # 1. Load Training data
    print("1. Load training data")
    data = loadmat('ex4data1.mat')
    X = data['X']
    y = data['y'].flatten()
    y = y - 1
    assert (y.max() == 9)
    assert (y.min() == 0)

    # 2. Load the weights into variables W1 and W2
    print("\n2. Load weights")
    weights = loadmat('ex4weights.mat')
    W1 = weights['Theta1']
    W2 = weights['Theta2']
    assert(W1.shape == (25, 401)), W1.shape
    assert(W2.shape == (10, 26)), W2.shape

    # Unroll parameters
    nn_params = np.hstack((W1.T.ravel(), W2.T.ravel()))
    assert(nn_params.shape == (10285,)), nn_params.shape

    # 3. Compute cost (Feedforward) with regularization
    print('\n3. Compute cost with regularization')
    print('\tFeedforward using Neural Network ...')
    J, grad = nnCostFunction(nn_params, input_size, hidden_size, n_labels, X, y, lmbda)

    print('\tCost at parameters (loaded from ex4weights): %f\n \
           (this value should be about 0.287629)\n' % J);

    # 4. Sigmoid gradient
    print('\n4. Test sigmoid gradient')
    test_sigmoid_gradient()

    # 5. Initialize Parameters
    print('\n5. Initialize parameters')
    initial_nn_params = init_weights(input_size, hidden_size, n_labels)

    # 6. Train NN
    print('\n6. Train NN')
    """
    scipy.optimize.fmin_cg(
                            f, x0, fprime=None, args=(), gtol=1e-05, norm=inf,
                            epsilon=1.4901161193847656e-08, maxiter=None, full_output=0,
                            disp=1, retall=0, callback=None
                          )
    Return:
        xopt : ndarray
            Parameters which minimize f, i.e. f(xopt) == fopt.
        fopt : float, optional
            Minimum value found, f(xopt). Only returned if full_output is True.
        func_calls : int, optional
            The number of function_calls made. Only returned if full_output is True.
        grad_calls : int, optional
            The number of gradient calls made. Only returned if full_output is True.
        warnflag : int, optional
            Integer value with warning status, only returned if full_output is True.
            0 : Success.
            1 : The maximum number of iterations was exceeded.
            2 : Gradient and/or function calls were not changing. May indicate
            that precision was lost, i.e., the routine did not converge.
    """
    xopt, fopt, func_calls, grad_calls, warnflag = fmin_cg(nnCost,
                         initial_nn_params,
                         fprime = nnGradient,
                         args = (input_size, hidden_size, n_labels, X, y, lmbda),
                         maxiter = 50,
                         full_output = True)
    print('%.4f' % fopt)

    W1_size = hidden_size * (input_size + 1)
    W1 = np.reshape(xopt[0 : W1_size], (hidden_size, input_size + 1), order = 'F')
    W2 = np.reshape(xopt[W1_size : ], (n_labels, hidden_size + 1), order = 'F')


    # 7. Predict
    print("\n7. Predict")
    print '\tw1.shape', W1.shape
    print '\tw2.shape', W2.shape
    print '\tx.shape', X.shape
    pred = predict(W1, W2, X)
    acc = np.mean(pred == y) * 100
    print('\tTraining Set Accuracy: %f\n' % acc)


if __name__ == '__main__':
    test_nn()