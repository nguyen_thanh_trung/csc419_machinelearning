import numpy as np

def randInitWeights(L_in, L_out):
    """
    Randomly initialize the weights of with L_in incoming connections
    and L_out outgoing connections

    W should be set to a matrix of size(L_out, 1 + L_in) as the column
    row of W handles the "bias" terms. The first row of W corresponds
    to the parameters for the bias units

    One effective strategy is to select values for W uniformly in the range [-ep, ep]
    A good choice of ep is sqrt(6) / sqrt(L_in + L_out)
    """

    epsilon = np.sqrt(6) / (np.sqrt(L_in + L_out))
    W = np.random.rand(L_out, 1 + L_in) * 2 * epsilon - epsilon

    return W

