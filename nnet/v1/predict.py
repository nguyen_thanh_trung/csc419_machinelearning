import numpy as np
from nn_helpers import *

def predict(W1, W2, X):
    """
    Predict the label

    @param w1 weight of the 1st layer
    @param w2 weight of the 2nd layer
    @param x input needs to be classified
    @return Predicted label of x given the trained weights
            of a neural network (w1, w2)
    """
    # X: n x m
    # w1: h x (m+1)
    # w2: o x (h+1)

    n = X.shape[0]
    a1 = np.hstack((np.ones((n, 1)), X))
    z2 = a1.dot(np.transpose(W1))
    a2 = sigmoid(z2)
    a2 = np.hstack((np.ones((a2.shape[0], 1)), a2))
    z3 = a2.dot(np.transpose(W2))
    a3 = sigmoid(z3)

    p = a3.argmax(axis = 1)

    return p
