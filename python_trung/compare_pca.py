import csv
import numpy as np

a = None
with open('input/iris.csv') as csv_file:
    iris_reader = csv.reader(csv_file, delimiter=',')
    first = True
    for row in iris_reader:
        if first:
            first = False
        else:
            if a is None:
                a = map(float, row)
            else:
                a = np.vstack((a, map(float, row)))
a = a.T
