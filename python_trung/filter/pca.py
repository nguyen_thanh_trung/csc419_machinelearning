from scipy import linalg
import numpy as np

from filter.generic_filter import GenericFilter
from matrix import MatrixUtils


class PCA(GenericFilter):
    @staticmethod
    def calculateFilter(X, n_dimension=None):
        """
        Get the PCA transformation matrix of X
        Each column should be an observation

        Return transformation matrix
        """

        # Get the covariance matrix
        covariance_matrix = MatrixUtils.cov(X)

        # Compute eigenvalues and eigenvectors
        eigenvalues, eigenvectors = linalg.eig(covariance_matrix)

        # Sort in descending order of eigenvalues
        indices = np.argsort(eigenvalues)[::-1]
        eigenvectors = eigenvectors[:, indices]

        if n_dimension is not None:
            eigenvectors = eigenvectors[:, :n_dimension]
        return eigenvectors.T

    @staticmethod
    def applyFilter(X, transformation_matrix):
        return np.dot(transformation_matrix, X)
