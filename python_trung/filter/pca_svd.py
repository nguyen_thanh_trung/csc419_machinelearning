from scipy import linalg
import numpy as np


class PCA():
    @staticmethod
    def applyPCA(X, n_dimension=None):
        """
        Get the PCA transformation matrix of X
        Each column should be an observation

        Return transformation matrix
        """

        # TODO: subtract mean

        U, s, Vt = linalg.svd(X, full_matrices=False)
        V = Vt.T

        ind = np.argsort(s)[::-1]
        U = U[:, ind]
        s = s[ind]
        V = V[:, ind]
        S = np.diag(s)

        print 'n_dimension = ', n_dimension
        return np.dot(S[:n_dimension, :n_dimension], V[:, :n_dimension].T)
