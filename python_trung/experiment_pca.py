import csv
import numpy as np
from filter.pca_svd import PCA as PCA_SVD
from filter.pca import PCA as PCA


nDim = 50
input_file = 'input/data.csv'
output_file = 'output/data.csv'
SVD_ON = False

face_data = None
face_label = []

with open(input_file) as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    firstRow = True
    for row in csv_reader:
        if firstRow:
            firstRow = False
        else:
            if face_data is None:
                face_data = map(float, row[:-1])
                face_label.append(row[-1])
            else:
                face_data = np.vstack((face_data, map(float, row[:-1])))
                face_label.append(row[-1])

face_data = face_data.T
print face_data.shape

if SVD_ON:
    pca_result = PCA_SVD.applyPCA(face_data, nDim).T
else:
    pca_result = PCA.applyFilter(face_data, PCA.calculateFilter(face_data, nDim)).T

print pca_result.ndim
print pca_result.shape

with open(output_file, 'w') as csv_file:
    csv_writer = csv.writer(csv_file, delimiter=',')
    header = []
    for i in xrange(nDim):
        header.append('x' + str(i))
    header.append('class')
    csv_writer.writerow(header)

    idx = 0
    for row in pca_result:
        csv_writer.writerow(row.tolist() + [str(face_label[idx])])
        idx += 1
