import csv
from matplotlib import pyplot
from matplotlib import cm
import numpy as np

input_file = 'input/data.csv'
data_face = []

with open(input_file) as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    idx = 0
    for row in csv_reader:
        idx += 1
        if idx == 1:
            pass
        else:
            data_face = np.array(map(float, row[:-1]))

data_face = data_face.reshape(92, 112)

pyplot.figure(1)
pyplot.pcolormesh(data_face, cmap=cm.Greys_r)
pyplot.figure(2)
face_fft = np.fft.fft2(data_face)
face_fft = face_fft / np.max(face_fft)
pyplot.pcolormesh(np.ma.masked_where(np.isnan(face_fft), face_fft))
pyplot.show()
