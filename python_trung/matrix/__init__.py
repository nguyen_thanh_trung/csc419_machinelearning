import numpy as np


class MatrixUtils:
    def __init__(self):
        pass

    @staticmethod
    def cov(X):
        """
        Return covariance matrix
        Input:
            X: 2D matrix
        Output:
            Covariance matrix
        """
        # Check if X is a 2D matrix with each dimension >= 2
        if X.ndim != 2:
            raise TypeError('X should be 2 dimensional array')
        if X.shape[0] < 2 or X.shape[1] < 2:
            raise TypeError('Each dimension of X should be at least 2')

        # Center data
        X -= X.mean(1) [::, None]

        # Return the covariance matrix
        return np.dot(X, X.T) / (X.shape[1] - 1)
