import csv
import numpy as np
from unittest import TestCase
from pca import PCA


class TestPCA(TestCase):
    def testIris(self):
        """
        Generate PCA transformation of Iris dataset.
        Should use WEKA to test the accuracy of generated output
        """
        a = None
        with open('input/iris.csv') as csv_file:
            iris_reader = csv.reader(csv_file, delimiter=',')
            first = True
            for row in iris_reader:
                if first:
                    first = False
                else:
                    if a is None:
                        a = map(float, row)
                    else:
                        a = np.vstack((a, map(float, row)))
        a = a.T

        nDim = 3
        columns = ['x', 'y', 'z']
        transform = PCA.calculateFilter(a, nDim)
        pca_result = PCA.applyFilter(a, transform).T
        with open('output/iris_pca_' + str(nDim) + '.csv', 'w') as csv_file:
            iris_writer = csv.writer(csv_file, delimiter=',')
            idx=0
            iris_writer.writerow(columns + ['class'])
            for row in pca_result:
                idx += 1
                if idx <= 50:
                    label = 'a'
                elif idx <= 100:
                    label = 'b'
                else:
                    label = 'c'
                iris_writer.writerow(row.tolist() + [label])
