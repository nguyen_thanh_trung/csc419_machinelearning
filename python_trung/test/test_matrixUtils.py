import numpy as np
from unittest import TestCase
from matrix import MatrixUtils


class TestMatrixUtils(TestCase):
    def testCov(self):
        for n_row in xrange(2, 40):
            for n_col in xrange(2, 40):
                x = np.random.rand(n_row, n_col)
                c_result = np.cov(x)
                c = MatrixUtils.cov(x)
                self.assertTrue(np.array_equal(c, c_result))
