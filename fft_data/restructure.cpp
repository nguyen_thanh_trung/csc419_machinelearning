#include <fstream>
#include <iostream>
using namespace std;

int main() {
    for(int turn = 0; turn < 2; ++turn) {
        fstream orig, out;
        if (turn == 0) {
            orig.open("edge_train.csv", fstream :: in);
            out.open("edge_train_good.csv", fstream :: out);
        }
        else {
            orig.open("edge_test.csv", fstream :: in);
            out.open("edge_test_good.csv", fstream :: out);
        }

        string s; orig >> s;
        int cnt = 0;
        for(int i = 0; i < s.length(); ++i)
            if (s[i] == ',')
                out << 'x' << (cnt++) << ',';
        out << 'x' << (cnt++) << ",class\n";

        int low, high;
        if (turn == 0) {
            low = 1; high = 4;
        }
        else {
            low = 5; high = 10;
        }

        for(int person = 1; person <= 40; ++person) {
            for(int img = low; img <= high; ++img) {
                out << s << ",c" << person << endl;
                orig >> s;
            }
        }
        out.close(); orig.close();
    }
}
